
*** Settings ***
Documentation
Library  Selenium2Library
#Test Setup  Login to IDIT as admin
#pybot -t "Create Accounts" IDIT/CreateUser.robot

*** Variables ***
${BROWSER}  chrome
${username}
${userNo}

*** Test Cases ***
Create Accounts
    Login to IDIT as admin
    :FOR  ${number}  IN RANGE  7  11
#    \  ${user} =  catenate  SEPARATOR=  testing  ${userNo}
#    \  set global variable  ${username}  ${user}
    \  Set Username  ${number}
    \  Create New User

Verify accounts
    open browser  about:blank  ${BROWSER}
    :FOR  ${number}  IN RANGE  56  110
    \  Set Username  ${number}
    \  Login to IDIT as user
    \  Logout IDIT
    close browser

Fix missing users
    Login to IDIT as admin
    @{missing}  create list  55  120
    :FOR  ${number}  IN  @{missing}
    \  set username  ${number}
    \  find contact
    \  create user

*** Keywords ***
Set Username
    [Arguments]  ${number}
    ${user} =  catenate  SEPARATOR=  testing  ${number}
    set global variable  ${username}  ${user}

Create New User
    Create Contact
    Create User

Login to IDIT as admin
    open browser   http://iditwas01:9084/idit-web/web-framework/login.do  ${BROWSER}
    input text  id=UserName  administrator
    input text  id=Password  2222
    click element  id=Login

Create Contact
#   search for contact
    wait until page contains element  id=DataDivMidPanel
    select from dropdown  //*[@id="s2id_legalFormID"]/a/span[2]/b  //*[@id="s2id_autogen1_search"]  Individual
    input text  id=IDITForm@name  ${username}
    input text  id=IDITForm@firstName  John
    input text  id=dateOfBirth  10/03/1981
    click element  id=homepageButtonsB
    wait until page contains element  id=NewContactNewGen
    click element  id=NewContactNewGen

#    create new contact
    wait until page contains element  id=s2id_IDITForm@entityTypeVO@id
    select from dropdown  //*[@id="s2id_Title"]/a/span[2]/b  //*[@id="s2id_autogen2_search"]  Mr.
#    input text  id=IDITForm@contactEmailForDisplay@email  testing@email.com

#    fill in address
    page should contain element  xpath=//*[@id="IDITForm@primaryAddressForDisplay@AddressCompoundUnitedKingdomDivId"]/fieldset/div/div/label[2]
    click element  xpath=//*[@id="IDITForm@primaryAddressForDisplay@AddressCompoundUnitedKingdomDivId"]/fieldset/div/div/label[2]
    sleep  1
    click element  id=IDITForm@primaryAddressForDisplay@GB@houseNr
#    input text  id=IDITForm@primaryAddressForDisplay@addressVO@apartmentNr  12
    input text  id=IDITForm@primaryAddressForDisplay@GB@houseNr  2${userNo}
#    input text  id=IDITForm@primaryAddressForDisplay@addressVO@pob  78
    input text  id=IDITForm@primaryAddressForDisplay@addressVO@addressLine1  Gatling
    input text  id=IDITForm@primaryAddressForDisplay@GB@streetID  Wising
    input text  id=IDITForm@primaryAddressForDisplay@GB@addressVO@cityName  London
    input text  id=IDITForm@primaryAddressForDisplay@addressVO@zipCode  A1 2CD
    click element  id=Next
    wait until page contains  duplicated with contact
    page should contain element  id=DialogOK
    click element  xpath=//*[@id="DialogOK"]/span
    wait until page contains  Finish
    page should contain element  id=footer
    click element  id=Finish
    wait until page contains  duplicated with contact
    click element  xpath=//*[@id="DialogOK"]/span
    wait until page contains  Saved Successfully
    click element  id=Ok

Create User
    wait until page contains  Settings
    click element  xpath=//*[@id="idit-header"]/nav/ul[2]/li[2]/a
    sleep  0.5
    click element  xpath=//*[@id="idit-header"]/nav/ul[2]/li[2]/div/ul/li[5]/a
    sleep  0.5
    click element  xpath=//*[@id="idit-header"]/nav/ul[2]/li[2]/div/ul/li[5]/ul/li[4]/a
    wait until page contains element  xpath=//*[@id="2000000"]/td[2]
    click element  xpath=//*[@id="2000000"]/td[2]
    click element  id=Next

#    org unit
    wait until page contains element  xpath=//*[@id="container"]/div[1]/ul/li[5]/a
    click element  xpath=//*[@id="container"]/div[1]/ul/li[5]/a
    wait until page contains element  xpath=//*[@id="DataDivId"]/fieldset/div[1]/div[1]/div/div/span[2]/i[2]
    click element  xpath=//*[@id="DataDivId"]/fieldset/div[1]/div[1]/div/div/span[2]/i[2]
    click element  id=searchOnOrgUnitTree
    wait until page contains element  xpath=//*[@id="idit-grid-table-flattendListselectedOrgUnitIdtreelist_pipe_"]/tbody/tr[2]/td
    click element  xpath=//*[@id="idit-grid-table-flattendListselectedOrgUnitIdtreelist_pipe_"]/tbody/tr[2]/td

#    element type
    wait until page contains  Element Type
    select from dropdown  //*[@id="s2id_IDITForm@contactRoleId"]/a/span[2]/b  //*[@id="s2id_autogen1_search"]  Staff
    click element  id=searchForOrganizations
    wait and click  xpath=//*[@id="flattendListfilterOrganizationVOList|addEmployee"]

#   select contact
    wait and click  id=findContact
    find contact
    wait and click  xpath=//*[@id="idit-grid-table-searchResult_pipe_"]/tbody/tr[2]
    click element  id=selectButtonSearch

#    org unit
    wait until page contains  Organizational Unit
    click element  //*[@id="DataDivId"]/fieldset/div[1]/div[3]/div/div/span/i[2]
    click element  id=searchOnOrgUnitTree
    wait until page contains element  xpath=//*[@id="idit-grid-table-flattendListselectedOrgUnitIdtreelist_pipe_"]/tbody/tr[2]/td
    click element  xpath=//*[@id="idit-grid-table-flattendListselectedOrgUnitIdtreelist_pipe_"]/tbody/tr[2]/td

#   position
    wait until page contains  Position
    select from dropdown  //*[@id="s2id_IDITForm@contactRoleId"]/a/span[2]/b  //*[@id="s2id_autogen1_search"]  Staff

#    Create user
    click element  id=newPerformSubAction(_createUserSubAction_)_Link
    wait until page contains  User Details
    input text  id=IDITForm@userVO@nameOfUser  ${username}
    select from dropdown  //*[@id="s2id_IDITForm@profileVO@id"]/a/span[2]/b  //*[@id="s2id_autogen2_search"]  Company Employee not limited
    input text  id=IDITForm@userVO@userAdditionalDataVO@userPassword  1111
    input text  id=additionalInfo(confirmPassword)  1111
    input text  id=IDITForm@userVO@discontinueDate  04/03/2026

#    add roles
    click element  id=ui-id-2
    add role  Admin
    add role  Master

#    confirm and go to homepage
    wait and click  id=OK
    wait and click  id=Ok
    wait and click  xpath=//*[@id="MainMenuFind_ImgLink"]/div

Select From Dropdown
    [Arguments]  ${dropdownElem}  ${lookupElem}  ${text}
    click element  xpath=${dropdownElem}
    input text  xpath=${lookupElem}  ${text}
    press key  xpath=${lookupElem}  \\13

Find Contact
    wait until page contains element  id=IDITForm@name
    input text  id=IDITForm@name  ${username}
    click element  id=homepageButtonsB
#    click element  id=selectButtonSearch

Wait and Click
    [Arguments]  ${elem}
    wait until page contains element  ${elem}
    click element  ${elem}

Add Role
    [Arguments]  ${role}
    wait until page contains   Role Name
    click element  id=userRolesList|addRoleToUserRolesTable
    wait until page contains element  id=additionalInfo(addNewUserRoleFilter)
    input text  id=additionalInfo(addNewUserRoleFilter)  ${role}
    click element  id=searchForRoles
    wait and click  xpath=//*[@id="idit-grid-table-currentUserRolesPVOListToAdd_pipe_"]/tbody/tr[2]/td[2]/label
    wait and click  xpath=//*[@id="idit-grid-table-currentUserRolesPVOListToAdd_pipe_"]/tbody/tr[2]/td[3]/label
    click element  id=OK

Login to IDIT as user
    go to   http://iditwas01:9084/idit-web/web-framework/login.do
    input text  id=UserName  ${username}
    input text  id=Password  1111
    click element  id=Login
    Page should contain  Contact by last Name

Logout IDIT
    click element  xpath=//*[@id="mainTopBar"]/div[2]/div/div[2]/button/span
    sleep  0.3
    wait and click  xpath=//*[@id="mainTopBar"]/div[2]/div/div[2]/ul/li[3]/a
    click element  xpath=//*[@id="DialogOK"]/span
    go to  http://iditwas01:9084/idit-web/web-framework/login.do
    page should contain  User name:
























