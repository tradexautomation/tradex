*** Settings ***

Documentation  This is the resource file containing all methods based on desktop page
Library        Selenium2Library


*** Variables ***


*** Keywords ***
Page desktop correctly loaded
    [Documentation]  Validate if the page was loaded

   # sleep  0.5
   # select window  url=http://iditwas01:9080/idit-web/web-framework/login.do
    wait until page contains  Settings
    wait until page contains element  IDITForm@firstName
    wait until page contains element  IDITForm@name
    wait until page contains element  select2-chosen-1
    wait until page contains element  dateOfBirth
    wait until page contains element  IDITForm@policyNumber
    wait until page contains element  IDITForm@claimNr
    wait until page contains element  IDITForm@licensePlate
    wait until page contains element  IDITForm@agentNumber

Log off
    wait until page contains element  quickSearchText
    click element   xpath=//*[@id="mainTopBar"]/div[2]/div/div[2]/button
    click element  xpath=//*[@id="mainTopBar"]/div[2]/div/div[2]/ul/li[3]/a
    wait until page contains  This change will remove data entered in this screen. Are you sure you want to proceed?
    click element  DialogOK


Click New
    [Documentation]  Click to the "New" button and select "New Contact"

    click element  xpath=//*[@id="idit-header"]/nav/ul[1]/li[4]/a
    wait until element is visible  xpath=//*[@id="NewContactNewGenFromMenu_Link"]
    click element  xpath=//*[@id="NewContactNewGenFromMenu_Link"]

Click New Quote
    [Documentation]  Click to the "New Quote"

    wait until page contains element  MainMenuPolicy_Link
    click element  MainMenuPolicy_Link

#Search contact ${Last/CompanyName} ${FirstName}

 #   click element  MainMenuFind_ImgLink
   # wait until page contains  Search
  #  wait until page contains element  IDITForm@name
  #  wait until page contains element  IDITForm@firstName
  #  input text  IDITForm@firstName   ${Last/CompanyName}
  #  input text  IDITForm@name   ${FirstName}
  #  click element  homepageButtonsB

Search contact by DOB ${DateOfBirth}

     click element  MainMenuFind_ImgLink
    wait until page contains  Search
    wait until page contains element  IDITForm@name
    wait until page contains element  IDITForm@firstName
    wait until page contains element  dateOfBirth
    input text  dateOfBirth  ${DateOfBirth}
    click element  homepageButtonsB
