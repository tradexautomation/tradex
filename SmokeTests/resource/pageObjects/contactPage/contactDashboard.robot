*** Settings ***

Documentation  This is the resource file containing all methods based on Contact Dashboard
Library        Selenium2Library


*** Variables ***


*** Keywords ***
Contact Dashboard correctly loaded

    wait until page contains element  ui-id-1
    wait until page contains element  xpath=//*[@id="DashboardTabContent"]/div[2]/div[2]/div[1]/div[2]
    wait until page contains element  xpath=//*[@id="DashboardTabContent"]/div[2]/div[2]/div[1]/div[3]
    wait until page contains  Current Portfolio
