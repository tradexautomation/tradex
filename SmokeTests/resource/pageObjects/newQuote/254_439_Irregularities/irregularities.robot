*** Settings ***

Documentation  This is the resource file containing all methods based on Maintain Policy Line of Business Vehicle Risk Objects page
Library        Selenium2Library

*** Variables ***


*** Keywords ***

Page Irregularities correctly loaded

    wait until page contains  Irregularities
    wait until page contains  Underwriter Notes
    wait until page contains element  authorizationExceptions|setAllAuthoritiesToApproval

Approve all irregularities

    click element  authorizationExceptions|setAllAuthoritiesToApproval
    wait until page contains  Approved


Approve and click next

    Page Irregularities correctly loaded
    Approve all irregularities
    Page Irregularities correctly loaded
    wait until page contains element  Next
    click element  Next






