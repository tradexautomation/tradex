*** Settings ***

Documentation  This is the resource file containing all methods based on New Quote Page
Library        Selenium2Library


*** Variables ***

*** Keywords ***

Product Name ${ProductName}

    wait until page contains element  xpath=//*[@id="s2id_Product"]
    click element  xpath=//*[@id="s2id_Product"]
    click element  xpath=//*[@id="Product"]/option[.='${ProductName}']

Intermediary ${Intermediary} ${Account}

    wait until page contains  Intermediary
    wait until page contains element  s2id_agentsList|1@contactVO@id
    click element  s2id_agentsList|1@contactVO@id
    click element  xpath=//*[@id="agentsList|1@contactVO@id"]/option[text()="${Intermediary}"]

    #Select account
    wait until page contains element  s2id_agentsList|1@agentAccountId
    click element  xpath=//*[@id="s2id_agentsList|1@agentAccountId"]
    click element  xpath=//*[@id="agentsList|1@agentAccountId"]/option[.='${Account}']

Click button Next

    wait until page contains element  Next
    click element  Next


Select product name ${ProductName} Intermediary ${Intermediary} account ${Account} and next

    Product Name ${ProductName}
    Intermediary ${Intermediary} ${Account}
    Click button Next