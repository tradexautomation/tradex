*** Settings ***

Documentation  This is the resource file containing all methods based on Business bookmark
Library        Selenium2Library
Library  String


*** Variables ***


*** Keywords ***

Page loaded correctly
    [Tags]  SpecifiedVehicle MotorTrade

    wait until page contains  Your Business
    wait until page contains element  s2id_IDITForm@yearsInBusinessTDXVO@id
    wait until page contains element  s2id_IDITForm@tradeFromTDXVO@id
    wait until page contains element  IDITForm@policyTypeVO@id

Select Established business or new venture ${EstablishedBusiness}
    [Tags]  SpecifiedVehicle MotorTrade

    click element  s2id_IDITForm@yearsInBusinessTDXVO@id
    click element   xpath=//*[@id="IDITForm@yearsInBusinessTDXVO@id"]/option[.='${EstablishedBusiness}']

Select Where is the client trading from ${ClientTradingFrom}
    [Tags]  SpecifiedVehicle MotorTrade

    click element  s2id_IDITForm@tradeFromTDXVO@id
    click element  xpath=//*[@id="IDITForm@tradeFromTDXVO@id"]/option[.='${ClientTradingFrom}']

Select Policy Type ${PolicyType}
    [Tags]  SpecifiedVehicle

    click element  s2id_policyType
    click element  xpath=//*[@id="policyType"]/option[.='${PolicyType}']

Select Main business activity
    [Tags]  SpecifiedVehicle

    ${Random} =  Generate Random String  2  [NUMBERS]
    click element  s2id_mainBusinessActivityId
    click element  xpath=//*[@id="mainBusinessActivityId"]/option[${Random}]

Approximate number of vehicles sold/repaired/handled in any one year
    [Tags]  MotorTrade

    input text  IDITForm@aproxNumOfVehicles  100

Estimated turnover for the coming year
    [Tags]  MotorTrade

    input text  IDITForm@estimatedTurnover  10000

Wage roll
    [Tags]  MotorTrade

    input text  IDITForm@wageRoll  70000


Click Next button
    [Tags]  SpecifiedVehicle MotorTrade

    click element  Next

Fill neccessary MT
    [Tags]  MotorTrade

    Page loaded correctly
    Select Established business or new venture ${EstablishedBusiness}
    Select Where is the client trading from ${ClientTradingFrom}
    Approximate number of vehicles sold/repaired/handled in any one year
    Estimated turnover for the coming year
    Wage roll


Fill neccessary SV
    [Tags]  SpecifiedVehicle

    Page loaded correctly
    Select Established business or new venture ${EstablishedBusiness}
    Select Where is the client trading from ${ClientTradingFrom}
    Select Policy Type ${PolicyType}
    Select Main business activity
    Click Next button