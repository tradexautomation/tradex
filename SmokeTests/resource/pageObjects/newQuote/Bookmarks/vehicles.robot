*** Settings ***

Documentation  This is the resource file containing all methods based on Maintain Policy Line of Business Vehicle Risk Objects page
Library        Selenium2Library
Library  String



*** Variables ***

${DriverPlan}      Any driver

*** Keywords ***

Page Vehicles correctly loaded
    [Tags]  SpecifiedVehicle

    wait until page contains  Filter Selection
    wait until page contains element  MotorTradeVehiclesTabContent
    wait until page contains element  Next


Click desired bookmark Vehicles
    [Tags]  SpecifiedVehicle

    click element  xpath=//*[@id="IDITFormId"]/div[1]/ul/li/div/a[.='Vehicles']

Click + button
     [Tags]  SpecifiedVehicle

    click element  IDITForm@assetVehicleTDXVOList|New

Go to Vehicles bookmark
    [Tags]  SpecifiedVehicle

    Click desired bookmark Vehicles
    Page Vehicles correctly loaded
    Click + button








