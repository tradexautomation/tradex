*** Settings ***

Documentation  This is the resource file containing all methods based on Maintain Policy Line of Business Vehicle Risk Objects page
Library        Selenium2Library
Library  String



*** Variables ***

@{L1}   YD10MHM  GL08YSR  SV11NUJ  HV08YJU  SF13WYH  LP60VEW  H15UHA  MV11TZZ  SF13WYH  J16LDB

${DriverPlan}      Any driver
${NumOfDrivers}    4

*** Keywords ***

Page drivers correctly loaded
    [Tags]  SpecifiedVehicle

    wait until page contains  Driver Plan
    wait until page contains  FON Obtained
    wait until page contains element  s2id_driverPlan

Select Driver Plan ${DriverPlan}
    [Tags]  SpecifiedVehicle

    click element  s2id_driverPlan
    click element  xpath=//*[@id="driverPlan"]/option[.='${DriverPlan}']


Wait until Any driver is loaded
    [Tags]  SpecifiedVehicle

    wait until page contains  Minimum length of licence
    wait until page contains  Acceptable convictions
    wait until page contains element  IDITForm@assetAnyDriverVOs|3@numberOfDrivers


Input number of drivers 25-29 ${NumOfDrivers}
    [Tags]  SpecifiedVehicle

    input text  IDITForm@assetAnyDriverVOs|3@numberOfDrivers  ${NumOfDrivers}

Click desired bookmark Drivers
    [Tags]  SpecifiedVehicle

    click element  xpath=//*[@id="IDITFormId"]/div[1]/ul/li/div/a[.='Drivers']

Fill in all neccessary drivers details
    [Tags]  SpecifiedVehicle

    Click desired bookmark Drivers
    Page drivers correctly loaded
    Select Driver Plan ${DriverPlan}
    Wait until Any driver is loaded
    Input number of drivers 25-29 ${NumOfDrivers}






