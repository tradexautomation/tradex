*** Settings ***

Documentation  This is the resource file containing all methods based on Business bookmark
Library        Selenium2Library
Library  String


*** Variables ***


*** Keywords ***

Page Business Activites loaded correctly
    [Tags]  MotorTrade

    wait until page contains  Your Business Activities
    wait until page contains element  IDITForm@businessActivityTDXVOList|1@percentage
    wait until page contains element  Next


Click desired bookmark Business Activities
    [Tags]  MotorTrade

    click element  xpath=//*[@id="IDITFormId"]/div[1]/ul/li/div/a[.='Business Activities']


Fill in Business Activity
    [Tags]  MotorTrade

    input text  IDITForm@businessActivityTDXVOList|1@percentage  100


Fill neccessary Business Activities
    [Tags]  MotorTrade

    Click desired bookmark Business Activities
    Page Business Activites loaded correctly
    Fill in Business Activity


