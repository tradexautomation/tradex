*** Settings ***

Documentation  This is the resource file containing all methods based on Business bookmark
Library        Selenium2Library
Library  String


*** Variables ***
${LevelOfCover}     Comprehensive
${Indemnity}        10,000

*** Keywords ***

Page Cover Requirements loaded correctly
    [Tags]  MotorTrade

    wait until page contains  Cover Requirement
    wait until page contains element  s2id_levelOfCoverId
    wait until page contains element  Next


Click desired bookmark Cover Requirements
    [Tags]  MotorTrade

    click element  xpath=//*[@id="IDITFormId"]/div[1]/ul/li/div/a[.='Cover Requirement']


Level of cover ${LevelOfCover}
    [Tags]  MotorTrade

    click element  s2id_levelOfCoverId
    click element  xpath=//*[@id="levelOfCoverId"]/option[.='${LevelOfCover}']

Indemnity limit own vehicles ${Indemnity}
    [Tags]  MotorTrade

    click element  s2id_indemnityLimitOwnID
    click element  //*[@id="indemnityLimitOwnID"]/option[.='${Indemnity}']


Maximum number of vehicles in your possession at any one time
    [Tags]  MotorTrade

    input text  IDITForm@numberOfVehicle  20



Fill neccessary Cover Requirement
    [Tags]  MotorTrade

    Click desired bookmark Cover Requirements
    Page Cover Requirements loaded correctly
    Level of cover ${LevelOfCover}
    Indemnity limit own vehicles ${Indemnity}
    Maximum number of vehicles in your possession at any one time

