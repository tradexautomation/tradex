*** Settings ***

Documentation  This is the resource file containing all methods based on Maintain Policy Line of Business Vehicle Risk Objects page
Library        Selenium2Library
Library  String



*** Variables ***

@{L1}   YD10MHM  GL08YSR  SV11NUJ  HV08YJU  SF13WYH  LP60VEW  H15UHA  MV11TZZ  SF13WYH  J16LDB

${VehicleKept}      Communal car park
${PresentValue}     1000
${DOP}              01/04/2016
*** Keywords ***

Page Vehicle Details correctly loaded
    [Tags]  SpecifiedVehicle

    wait until page contains  Registration
    wait until page contains  VIN
    wait until page contains  Model
    wait until page contains element  licensePlate

Input Registration
    [Tags]  SpecifiedVehicle

    ${Random} =  Generate Random String  1  [NUMBERS]
    input text  licensePlate  @{L1}[${Random}]

Click Search Vehicle
    [Tags]  SpecifiedVehicle

    click element  searchVehicle
    wait until page contains  CARS(exc. Off-Road)

Select Where is the vehicle kept ${VehicleKept}
    [Tags]  SpecifiedVehicle

    click element  s2id_IDITForm@lockedParkingVO@id
    click element  xpath=//*[@id="IDITForm@lockedParkingVO@id"]/option[.='${VehicleKept}']

Input Present Value ${PresentValue}
    [Tags]  SpecifiedVehicle

    input text  IDITForm@vehicleValueVO@amount  ${PresentValue}

Input Date of purchase ${DOP}
    [Tags]  SpecifiedVehicle

    input text  IDITForm@dateOfPurchase  ${DOP}



Fill in all neccessary vehicle details
    [Tags]  SpecifiedVehicle

    Page Vehicle Details correctly loaded
    Input Registration
    Click Search Vehicle
    Select Where is the vehicle kept ${VehicleKept}
    Input Present Value ${PresentValue}
    Input Date of purchase ${DOP}



