*** Settings ***

Documentation  This is the resource file containing all methods based on Maintain Policy Line of Business Vehicle Risk Objects page
Library        Selenium2Library
Library  String



*** Variables ***

${CollectionMethod}     Broker Collected
${PaymentMethod}        Broker Collected

*** Keywords ***

Page CD correctly loaded

    wait until page contains  Payments
    wait until page contains  Premium breakdown
    wait until page contains  Decision
    wait until page contains element  s2id_CollectionMethodId
    wait until page contains element  Next

Input Collection Method ${CollectionMethod}

    click element  s2id_CollectionMethodId
    click element  xpath=//*[@id="CollectionMethodId"]/option[.='${CollectionMethod}']


Input Payment Terms ${PaymentMethod}

    click element  s2id_paymentTermId
    click element  xpath=//*[@id="paymentTermId"]/option[.='${PaymentMethod}']



Happy path Customer Decision

    Page CD correctly loaded
    Input Collection Method ${CollectionMethod}
    Input Payment Terms ${PaymentMethod}
    Page CD correctly loaded
    click element  Next





