*** Settings ***

Documentation  This is the resource file containing all methods based on covers
Library        Selenium2Library
Library  String


*** Variables ***


*** Keywords ***

Click End cover selection

    wait until page contains element  flattendListflattenedPolicyItems|ExportToExcel
    wait until page contains  Total Premium
    wait until page contains element  Finish
    click element  Finish
