*** Settings ***

Documentation  This is the resource file containing all methods based on Maintain Policy Line of Business Vehicle Risk Objects page
Library        Selenium2Library



*** Variables ***


*** Keywords ***
Page LOB Risk Objects correctly loaded

    wait until page contains  LOB Risk Objects
    wait until page contains element  t_idit-grid-table-flattendListflattenedPolicyItems_pipe_
    wait until page contains  Search by Customer Risk Object Number

Click Add

    click element  xpath=//*[@id="flattendListflattenedPolicyItems|New"]

Happy path Vehicle Risk Objects click Add

    Page LOB Risk Objects correctly loaded
    Click Add

