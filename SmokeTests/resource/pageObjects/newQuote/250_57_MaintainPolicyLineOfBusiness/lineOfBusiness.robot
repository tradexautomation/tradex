*** Settings ***

Documentation  This is the resource file containing all methods based on Maintaint Policy Line of Business page
Library        Selenium2Library


*** Variables ***

*** Keywords ***

Double click Business

    wait until page contains element  xpath=//*[@id="idit-grid-table-flattendListflattenedPolicyItems_pipe_"]
    wait until page contains  Line of Business
    double click element  xpath=//*[@class="ui-widget-content jqgrow ui-row-ltr level1"]/td['Business']


Double click Vehicles

    wait until page contains element  xpath=//*[@id="idit-grid-table-flattendListflattenedPolicyItems_pipe__innerVO@presentationDesc"]
    wait until page contains  Line of Business
    wait until page contains  Total Premium
    wait until page contains element  xpath=//tbody//tr[@class="ui-widget-content jqgrow ui-row-ltr level1"][2]
    wait until page contains element  xpath=//*[@class="ui-widget-content jqgrow ui-row-ltr level1"]/td['Business']
    double click element  xpath=//tbody//tr[@class="ui-widget-content jqgrow ui-row-ltr level1"][2]


Double click Motor Trade

    wait until page contains element  xpath=//*[@id="idit-grid-table-flattendListflattenedPolicyItems_pipe_"]
    wait until page contains  Line of Business
    double click element  xpath=//*[@class="ui-widget-content jqgrow ui-row-ltr level1"]/td['Motor Trade']



Click End risk details

    wait until page contains element  xpath=//*[@id="idit-grid-table-flattendListflattenedPolicyItems_pipe_"]
    wait until page contains element  xpath=//*[@id="idit-grid-table-flattendListflattenedPolicyItems_pipe__innerVO@presentationDesc"]
    wait until page contains element  next
    click element  next