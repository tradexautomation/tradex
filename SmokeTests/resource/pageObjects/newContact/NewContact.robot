*** Settings ***

Documentation  A smoke test for creating new Individual contact beginning
Library        Selenium2Library


*** Variables ***


*** Keywords ***
Page New Contact corectly loaded

    wait until page contains element  s2id_IDITForm@entityTypeVO@id
    wait until page contains element  LastName

Select Legal Type ${LegalType}

    click element  s2id_IDITForm@entityTypeVO@id
    click element  xpath=//*[@id="IDITForm@entityTypeVO@id"]/option[.='${LegalType}']


Input Last/Company Name ${Last/CompanyName}

    input text  LastName  ${Last/CompanyName}

Input First Name ${FirstName}

    input text  FirstName  ${FirstName}

Select Title ${Title}

    click element  s2id_Title
    click element  xpath=//*[@id="Title"]/option[.='${Title}']

Select Gender ${Gender}

    click element  s2id_Gender
    click element  xpath=//*[@id="Gender"]/option[.='${Gender}']

Input Date of Birth ${DateOfBirth}

    input text  dateOfBirth  ${DateOfBirth}

Fill in adress manually

    click element  xpath=//label[@for="IDITForm@primaryAddressForDisplay@GB@manualAddressFlag"]
    #or click element xpath=//*[@id="IDITForm@primaryAddressForDisplay@AddressCompoundUnitedKingdomDivId"]/fieldset/div/div/label[2]
    input text  id=IDITForm@primaryAddressForDisplay@GB@houseNr  ${House}
    input text  id=IDITForm@primaryAddressForDisplay@GB@addressVO@cityName  ${City}
    input text  id=IDITForm@primaryAddressForDisplay@addressVO@zipCode  ${PostCode}


Finish Contact

    click button  Finish


Check Contact Created Successfully and click ok

    wait until page contains  Saved Successfully
    click element  Ok

#Click Search Adress

  #  click element  IDITForm@primaryAddressForDisplay@GB@unitedKingdomAddEditBtn
  #  wait until page contains  Postcode
 #   wait until page contains  House
  #  input text  IDITForm@primaryAddressForDisplay@GB@searchZipCode  ${Postcode}
  #  input text  IDITForm@primaryAddressForDisplay@GB@searchHouseNr  ${House}



Create Individual Contact
    [Documentation]  Input data about contact and click finish

    Page New Contact corectly loaded
    Select Legal Type ${LegalType}
    wait until page contains  First Name
    Input Last/Company Name ${Last/CompanyName}
    Input First Name ${FirstName}
    Select Title ${Title}
    Select Gender ${Gender}
    Input Date of Birth ${DateOfBirth}
    Fill in adress manually
    Finish Contact
    Check Contact Created Successfully and click ok
