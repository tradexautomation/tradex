*** Settings ***

Documentation  This is the resource file containing all methods based on login page
Library        Selenium2Library



*** Variables ***

${Virgil}        iditwas01:9080/
${Jeff}          iditwas01:9085/
${DELAY}         0
${SERVER}        ${Virgil}
${LOGIN URL}     http://${SERVER}idit-web/web-framework/login.do
${BROWSER}       chrome
${login}         VSvoboda
${Password}      1111

*** Keywords ***

Login as Admin
    input text  UserName  ${login}
    input text  Password  ${Password}

Open Browser To Login Page
    Open Browser  ${LOGIN URL}  ${BROWSER}
    Maximize Browser Window
    Set Selenium Speed  ${DELAY}
    Title Should Be  Sapiens IDIT

Submit Credentials
    Click Button  Login

Wait until page contains all elements for login
    wait until page contains element  UserName
    wait until page contains element  Password
    wait until page contains element  Station
    wait until page contains element  Login

#Check if Login was succesful


#Check there is no warning about login


Login correct
    [Documentation]  Login into application

    Open Browser To Login Page
    Wait until page contains all elements for login
    Login as Admin
    Submit Credentials

