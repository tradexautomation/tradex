*** Settings ***
Documentation  A smoke test for creating new Individual contact

Resource  ../../tradex/SmokeTests/resource/pageObjects/loginPage.robot
Resource  ../../tradex/SmokeTests/resource/pageObjects/desktop/desktopPage.robot
Resource  ../../tradex/SmokeTests/resource/pageObjects/newContact/newContact.robot
Resource  ../../tradex/SmokeTests/resource/pageObjects/contactPage/contactDashboard.robot
Resource  ../../tradex/SmokeTests/resource/pageObjects/newQuote/250_240_MaintainPolicyQuoteGeneralDetails/newQuote.robot
Resource  ../../tradex/SmokeTests/resource/pageObjects/newQuote/250_57_MaintainPolicyLineOfBusiness/lineOfBusiness.robot
Resource  ../../tradex/SmokeTests/resource/pageObjects/newQuote/Bookmarks/business.robot
Resource  ../../tradex/SmokeTests/resource/pageObjects/newQuote/maintainPolicyLineOfBusiness/covers.robot
Resource  ../../tradex/SmokeTests/resource/pageObjects/newQuote/maintainPolicyLineOfBusiness/vehicleRiskObject.robot
Resource  ../../tradex/SmokeTests/resource/pageObjects/newQuote/Bookmarks/vehicleDetails.robot
Resource  ../../tradex/SmokeTests/resource/pageObjects/newQuote/Bookmarks/drivers.robot
Resource  ../../tradex/SmokeTests/resource/pageObjects/newQuote/254_439_Irregularities/irregularities.robot
Resource  ../../tradex/SmokeTests/resource/pageObjects/newQuote/maintainPolicyLineOfBusiness/customerDecision.robot
Resource  ../../tradex/SmokeTests/resource/pageObjects/newQuote/252_72_MaintainPolicyContact/policyContact.robot
Resource  ../../tradex/SmokeTests/resource/pageObjects/newQuote/262_44_MaintainEvent/maintainEvent.robot
Resource  ../../tradex/SmokeTests/resource/pageObjects/newQuote/Bookmarks/businessActivities.robot
Resource  ../../tradex/SmokeTests/resource/pageObjects/newQuote/Bookmarks/coverRequirement.robot
Resource  ../../tradex/SmokeTests/resource/pageObjects/newQuote/Bookmarks/vehicles.robot




#Test Teardown  Run keywords  desktopPage.Log off
#pybot SmokeTests/MotorTrade.robot

*** Variables ***
${Space}


${Last/CompanyName}     Automat.
${FirstName}            wht
${DateOfBirth}          03/10/1959
${ProductName}          Motor Trade
${Intermediary}         TRADEX INSURANCE COMPANY LIMITED${Space} MILLENNIUM STADIUM WESTGATE STREET CARDIFF CF10 1NS
${Account}              Intermediary TRADEX INSURANCE COMPANY LIMITED 57
${EstablishedBusiness}  Established business
${ClientTradingFrom}    Home
${PolicyType}           Commercial vehicle




*** Test Cases ***

Create Specified Vehicle Policy
    loginPage.Login correct
    desktopPage.Page desktop correctly loaded
   #momentalne nefunguje z nejaoeho duvodu vyhledavani
   #desktopPage.Search contact ${Last/CompanyName} ${FirstName}
    desktopPage.Search contact by DOB ${DateOfBirth}
    desktopPage.Click New Quote
    newQuote.Select product name ${ProductName} Intermediary ${Intermediary} account ${Account} and next
    lineOfBusiness.Double click Motor Trade
    business.Fill neccessary MT
    businessActivities.Fill neccessary Business Activities
    coverRequirement.Fill neccessary Cover Requirement
    drivers.Fill in all neccessary drivers details
    vehicles.Go to Vehicles bookmark
    vehicleDetails.Fill in all neccessary vehicle details
    click element  OK






