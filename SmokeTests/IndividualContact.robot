*** Settings ***
Documentation  A smoke test for creating new Individual contact

Resource  ../../tradex/SmokeTests/resource/pageObjects/loginPage.robot
Resource  ../../tradex/SmokeTests/resource/pageObjects/desktop/desktopPage.robot
Resource  ../../tradex/SmokeTests/resource/pageObjects/newContact/newContact.robot
Resource  ../../tradex/SmokeTests/resource/pageObjects/contactPage/contactDashboard.robot

#Test Teardown  Run keywords  desktopPage.Log off
#pybot SmokeTests/IndividualContact.robot

*** Variables ***
${LegalType}            Individual
${Last/CompanyName}    Automat.
${FirstName}            wht
${Gender}               Male
${DateOfBirth}          03/10/1961
${Postcode}             E12 6JZ
${House}                27
${City}                 London
${Title}                Dr.

*** Test Cases ***

Create contact
    loginPage.Login correct
    desktopPage.Page desktop correctly loaded
    desktopPage.Click New
    newContact.Create Individual Contact
    contactDashboard.Contact Dashboard correctly loaded
